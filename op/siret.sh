#!/bin/sh

API_ENDPOINT="entreprises/sirene/V3"
_op="siret"
SIRETLEN=14

_SIRET="$1"
_idx=$(expr "${_SIRET}" : "[0-9]*")
if [ -z "${_SIRET}" -o ${#_SIRET} -ne ${SIRETLEN} -o ${_idx} -ne ${SIRETLEN} ]; then
    echo "sh api.sh siret SIRET"
    exit 1
fi

_opcmd="${_op}/${_SIRET}"
