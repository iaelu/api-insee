#!/bin/sh

API_ENDPOINT=""
_op="token"

if [ ${_apiepoq} -lt ${API_EXPIRE_EPOQ} ]; then
    echo "Info: token has not expired"
    exit 0
fi

HEADCMD="/usr/bin/head"
B64ENCCMD="/usr/bin/b64encode"
SYSRCCMD="/usr/sbin/sysrc"

_B64SECRET=$(echo "${API_CONSUMER_KEY}:${API_CONSUMER_SECRET}" | ${B64ENCCMD} -m -r -o /dev/stdout token | ${HEADCMD} -n1)

_CURL_HEADER_ADD "Authorization: Basic ${_B64SECRET}"
_CURL_DATA_ADD "grant_type=client_credentials"

_opcmd="${_op}"
_CURL_OUTPUT=$(mktemp /tmp/api-insee-${_op}-json.XXXX)
_oppostcmd="token_postcmd"

token_postcmd() {
    local _token _type _expire _epoq

    _expire=$(cat ${_CURL_OUTPUT} | ${JQCMD} -r '.expires_in')
    _epoq=$(date -j -v +${_expire}S +%s)
    _token=$(cat ${_CURL_OUTPUT} | ${JQCMD} -r '.access_token')
    _type=$(cat ${_CURL_OUTPUT} | ${JQCMD} -r '.token_type')

    ${SYSRCCMD} -f ${_api_dir}/api.conf "API_ACCESS_TOKEN=${_token}" 
    ${SYSRCCMD} -f ${_api_dir}/api.conf "API_TOKEN_TYPE=${_type}" 
    ${SYSRCCMD} -f ${_api_dir}/api.conf "API_EXPIRE_EPOQ=${_epoq}" 

    rm -f ${_CURL_OUTPUT}

    return 0
}
