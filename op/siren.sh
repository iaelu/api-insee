#!/bin/sh

API_ENDPOINT="entreprises/sirene/V3"
_op="siren"
SIRENLEN=9

_SIREN="$1"
_idx=$(expr "${_SIREN}" : "[0-9]*")
if [ -z "${_SIREN}" -o ${#_SIREN} -ne ${SIRENLEN} -o ${_idx} -ne ${SIRENLEN} ]; then
    echo "sh api.sh siren SIREN"
    exit 1
fi

_opcmd="${_op}/${_SIREN}"
