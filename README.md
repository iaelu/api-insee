# api-insee

## Description

`api-insee` is a very simple set of shell scripts that enables someone to do very simple request on the INSEE opendata sources, see https://api.insee.fr/ for more details.
These scripts have been developed on FreeBSD and to use its base, with the addition of 2 packages:
* `ftp/curl`
* `textproc/jq`

## Configuration

This set of shell scripts requires to be configured with 2 values in the `api.conf` file. These values can be obtained by signing-up to https://api.insee.fr/.

## Commands
### token

The `token` command requests the INSEE API for the `Authorization: Bearer` to be generated. By default, the token lasts 7 days. This should be considered as the 1st command to run for the others to work correctly.

### siren

The `siren` command requests the INSEE API for a company's main informations. This command requires 1 parameter, the SIREN of the company.

### siret

The `siret` command requests the INSEE API for a company's institution information. This command requires 1 parameter, the SIRET of the company's institution.

### informations

The `informations` command requests the INSEE API for its status informations.

## Usage

```
sh api.sh command [parameter]
```
