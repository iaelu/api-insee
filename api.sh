#!/bin/sh

_api_dir=$(dirname $(realpath $0))

. ${_api_dir}/api.subr
. ${_api_dir}/api.conf

_apiop=""
case "$1" in
    token)
        _apiop="$1"
        ;;
    siren)
        _apiop="$1"
        shift
        ;;
    siret)
        _apiop="$1"
        shift
        ;;
    info|informations)
        _apiop="informations"
        ;;
    *)
        echo 1>&2 "usage: sh api.sh command [parameter]"
        exit 64
        ;;
esac

_apiepoq=$(date -j +%s)
if [ ${_apiepoq} -ge ${API_EXPIRE_EPOQ} -a "${_apiop}" != "token" ]; then
    echo "Warning: token has expired, consider running 'sh api.sh token'"
fi

_CURL_PARAMS_CLEAR
if [ "${_apiop}" != "token" ]; then
    _CURL_HEADER_ADD "Authorization: ${API_TOKEN_TYPE} ${API_ACCESS_TOKEN}"
fi
. ${_api_dir}/op/${_apiop}.sh

if ! _CURL_GET ${_opcmd}; then
    echo 1>&2 "CURL EXIT CODE: ${_CURL_EXIT_CODE}"
    exit 1
fi
if [ -n "${_oppostcmd}" ]; then
    eval "${_oppostcmd}"
    _ret=$?
    if [ ${_ret} -ne 0 ]; then
        echo "${_oppostcmd}: failed"
        exit 1
    fi
fi
